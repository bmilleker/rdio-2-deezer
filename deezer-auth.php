<?php
    include_once("includes/conf.php");  
    
    if (empty($_GET['code'])) {
        $auth_url = "https://connect.deezer.com/oauth/auth.php?app_id=" . constant("DEEZER_CLIENT_ID") . "&redirect_uri=" . urlencode(constant("DEEZER_AUTH_URL")) . "&perms=basic_access,email,manage_library";
        header('Location: ' . $auth_url);
        exit;
        
    } else {
        // code found
        $code = $_GET['code'];  

        // get the access token with curl request
        $code_url = "https://connect.deezer.com/oauth/access_token.php?app_id=" . constant("DEEZER_CLIENT_ID") . "&secret=" . constant("DEEZER_CLIENT_SECRET") . "&code=" . $code;

        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $code_url); 
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        $output = curl_exec($ch); 

        print_r($output);
        
        curl_close($ch);   
    }
?>