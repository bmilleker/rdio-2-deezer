<?php
    include_once("includes/conf.php");  
    
    if (empty($_GET['code'])) {
        $auth_url = "http://www.rdio.com/oauth2/authorize?response_type=code&client_id=" . constant("RDIO_CLIENT_ID") . "&redirect_uri=" . urlencode(constant("RDIO_AUTH_URL"));
        header('Location: ' . $auth_url);
        
    } else {
        // code found
        $code = $_GET['code'];  
        
        // get the access token with curl request
        $code_url = "https://services.rdio.com/oauth2/token?grant_type=authorization_code&client_id=" . constant("RDIO_CLIENT_ID") . "&client_secret=" . constant("RDIO_CLIENT_SECRET") . "&code=" . $code . "&redirect_uri=" . urlencode(constant("RDIO_AUTH_URL"));

        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $code_url); 
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        $output = curl_exec($ch); 

        echo "<pre>";
        print_r(json_decode($output));
        
        curl_close($ch);   
    }
?>
