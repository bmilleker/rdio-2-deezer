<?php
    include_once("includes/conf.php");  
        
    $albums_url = "https://services.rdio.com/api/1/?access_token=" . constant("RDIO_ACCESS_TOKEN");
    
    $ch = curl_init(); 
    curl_setopt($ch, CURLOPT_URL, $albums_url); 
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,
            "method=getFavorites&types=tracksAndAlbums&sort=name&count=999999999");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
    $output = curl_exec($ch); 
    print_r($output);
    $album = json_decode($output);
    $data = array();
    
    foreach($album->result as $a) {
        $data[] = $a->artist . "|" . $a->name;
    }
    
    file_put_contents(constant("LIBRARY_CSV"), implode("\n", $data) , FILE_APPEND);
?>