<?php  
    include_once("includes/conf.php");  
    
    $file_arr = file(constant("LIBRARY_CSV"));
    $album_added_ids = array();
    
    // get user ID 
    $user_info = json_decode(curlGET("http://api.deezer.com/user/me?access_token=" . constant("DEEZER_ACCESS_TOKEN")));
    $user_id = $user_info->id;
    
    
    
    foreach ($file_arr as $f) {
        $csv = explode("|", $f);
        
        // find the artist
        $artists = json_decode(curlGET('http://api.deezer.com/search/artist?q=' . urlencode($csv[0])));
        
        
        foreach ($artists->data as $a) {
            // fetch artists albums
            $artist_id = $a->id;
            $artist_albums = json_decode(curlGET("http://api.deezer.com/artist/" . $artist_id . "/albums"));
            $match = false;
                        
            foreach ($artist_albums->data as $aa) {
                $album_name = $aa->title;
                $album_id = $aa->id;  
                $csv_album_name = trim($csv[1]);
            
                $lev = levenshtein(trim($csv_album_name), $album_name);
                echo "{$csv_album_name} VS {$album_name}: {$lev}" . PHP_EOL; 
                
                // compare to csv record
                if ($lev <= 3) {
                    echo ">>> Match found!" . PHP_EOL;
                    $match = true;
                    
                    // add album to library
                    $post = array("album_id" => $album_id);
                    $album_added = json_decode(curlPOST("http://api.deezer.com/user/{$user_id}/albums?&access_token=" . constant("DEEZER_ACCESS_TOKEN"), $post, $headers));
                    
                    //print_r($album_added);
                }
            }
            
            if (!$match) {
                echo "!!! No match found!" . PHP_EOL;
            }
        }
    }
?>