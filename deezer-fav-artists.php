<?php  
    include_once("includes/conf.php");  
    
    $file_arr = file(constant("LIBRARY_CSV"));
    $album_added_ids = array();
    
    // get user ID 
    $user_info = json_decode(curlGET("http://api.deezer.com/user/me?access_token=" . constant("DEEZER_ACCESS_TOKEN")));
    $user_id = $user_info->id;
    
    
    
    foreach ($file_arr as $f) {
        $csv = explode("|", $f);
        
        // find the artist
        $artists = json_decode(curlGET('http://api.deezer.com/search/artist?q=' . urlencode($csv[0])));
        
        
        foreach ($artists->data as $a) {
            // fetch artists albums
            $artist_id = $a->id;
            $artist_name = $a->name;
            $csv_artist_name = $csv[0];
            
            //print_r($a);
            
            // compare artist name to csv
            $lev = levenshtein(trim($csv_artist_name), $artist_name);
            echo "{$csv_artist_name} VS {$artist_name}: {$lev}" . PHP_EOL; 
            
            // compare to csv record
            if ($lev <= 3) {
                echo ">>> Match found!" . PHP_EOL;
                $match = true;

                // fav artist
                $post = array("artist_id" => $artist_id);
                $artist_added = json_decode(curlPOST("http://api.deezer.com/user/{$user_id}/artists?&access_token=" . constant("DEEZER_ACCESS_TOKEN"), $post, $headers));
            }
        }
    }
?>