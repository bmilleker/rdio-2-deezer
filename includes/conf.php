<?php
    define("DEEZER_CLIENT_ID", "");
    define("DEEZER_CLIENT_SECRET", ""); 
    define("DEEZER_ACCESS_TOKEN", "");
    define("DEEZER_AUTH_URL", "http://somedomain.local/deezer-auth.php");

    define("RDIO_CLIENT_ID", "");
    define("RDIO_CLIENT_SECRET", ""); 
    define("RDIO_ACCESS_TOKEN", "");
    define("RDIO_AUTH_URL", "http://somedomain.local/rdio-auth.php");

    define("LIBRARY_CSV", "data/library.csv");
    
    
    function curlGET($url) {
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        $r = curl_exec($ch); 
        curl_close($ch);
        
        return $r;
    }
    
    function curlPOST($url, $data, $headers) {
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $r = curl_exec($ch); 
        curl_close($ch);
        
        return $r;
    }
?>